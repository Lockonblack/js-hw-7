const arr = [
    "Ivan",
    24,
    "24",
    null,
    undefined,
    "Kiev",
    "audi"
];
function filterBy(arr,typeDataArray){
    return arr.filter(item => typeof item !== typeDataArray)
}
const filterArr = filterBy(arr, 'string');
console.log(filterArr)